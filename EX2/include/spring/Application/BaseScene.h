#pragma once
#include <spring\Framework\IScene.h>
#include <qpushbutton.h>
#include <qlabel>
#include <qlineedit.h>
#include <qobject.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
		

	private:
		void createGUI();
		QWidget *centralWidget;
		QLabel *label;
		QPushButton *backButton;

		private slots:
		void mf_backButton();
	};

}
