#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <string>

const std::string initialSceneName = "SceneNumber1";
const std::string baseSceneName = "SceneNumber";
const int maxSceneNumber = 1000;

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{
	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);
		IScene* baseScene;

		for (int i = 2; i <= maxSceneNumber; i++)
		{
			std::string sceneNumber = baseSceneName;
			sceneNumber.append(std::to_string(i));
			baseScene = new BaseScene(sceneNumber);

			m_Scenes.emplace(sceneNumber, baseScene);
		}
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		//add initial values for all transient data 

		int sceneNumber = 1;
		m_TransientData.emplace("SceneNumber", sceneNumber);

	}
}
