#pragma once
#include <spring\Framework\IScene.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qobject.h>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QLineEdit *lineEdit;
		QPushButton *nextButton;
		QPushButton *previousButton;

		private slots:
		void mf_nextButton();
		void mf_previousButton();
	};
}
