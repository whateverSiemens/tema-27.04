#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(pushButton, SIGNAL(released()), this, SLOT(mf_pushButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{

		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("m_uMainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		lineEdit = new QLineEdit(centralWidget);
		lineEdit->setObjectName(QStringLiteral("lineEdit"));
		lineEdit->setGeometry(QRect(100, 100, 161, 20));
		pushButton = new QPushButton(centralWidget);
		pushButton->setObjectName(QStringLiteral("pushButton"));
		pushButton->setGeometry(QRect(140, 140, 75, 23));
		m_uMainWindow->setCentralWidget(centralWidget);

		m_uMainWindow->setWindowTitle(QApplication::translate("m_uMainWindow", "Main Window", Q_NULLPTR));
		pushButton->setText(QApplication::translate("m_uMainWindow", "Say Hello!", Q_NULLPTR));
	}

	void InitialScene::mf_pushButton()
	{
		const std::string personName = lineEdit->text().toStdString();

		if (!personName.empty())
		{
			m_TransientDataCollection.erase("PersonName");
			m_TransientDataCollection.emplace("PersonName", personName);

			
		}
		else
		{
			std::string stranger("Stranger");
			m_TransientDataCollection.erase("PersonName");
			m_TransientDataCollection.emplace("PersonName", stranger);
		}
		const std::string c_szNextSceneName = "BaseScene";


		emit SceneChange(c_szNextSceneName);
	}
}

