#include "..\..\..\include\spring\Application\BaseScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		
		m_uMainWindow->setWindowTitle(QString("Hello"));
		createGUI();

		QObject::connect(backButton, SIGNAL(released()), this, SLOT(mf_backButton()));
		
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("m_uMainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		label = new QLabel(centralWidget);


		std::string personName = boost::any_cast<std::string>(m_TransientDataCollection["PersonName"]);
		label->setText(QString("Hello, ").append(QString::fromStdString(personName)));

		label->setGeometry(QRect(100, 100, 75, 23));
		backButton = new QPushButton(centralWidget);
		backButton->setObjectName(QStringLiteral("pushButton"));
		backButton->setGeometry(QRect(140, 140, 75, 23));
		m_uMainWindow->setCentralWidget(centralWidget);


		m_uMainWindow->setWindowTitle(QApplication::translate("m_uMainWindow", "Main Window", Q_NULLPTR));
		backButton->setText(QApplication::translate("m_uMainWindow", "Back!", Q_NULLPTR));
	}

	void BaseScene::mf_backButton()
	{
		const std::string c_szNextSceneName = "InitialScene";
		

		emit SceneChange(c_szNextSceneName);
	}
}
