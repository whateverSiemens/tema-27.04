#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("m_uMainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		label->setGeometry(QRect(16, 12, 131, 71));
		m_uMainWindow->setCentralWidget(centralWidget);

		m_uMainWindow->setWindowTitle(QApplication::translate("m_uMainWindow", "Main Window", Q_NULLPTR));
		label->setText(QApplication::translate("m_uMainWindow", "Hello world!", Q_NULLPTR));
	}

	
}

