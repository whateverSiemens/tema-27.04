#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(nextButton, SIGNAL(released()), this, SLOT(mf_nextButton()));
		QObject::connect(previousButton, SIGNAL(released()), this, SLOT(mf_previousButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		if (m_uMainWindow->objectName().isEmpty())
			m_uMainWindow->setObjectName(QStringLiteral("m_uMainWindow"));
		m_uMainWindow->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		lineEdit = new QLineEdit(centralWidget);
		lineEdit->setObjectName(QStringLiteral("lineEdit"));
		int currentSceneNumber = boost::any_cast<int>(m_TransientDataCollection["SceneNumber"]);
		lineEdit->setText(QString::number(currentSceneNumber));
		lineEdit->setGeometry(QRect(110, 120, 161, 20));
		nextButton = new QPushButton(centralWidget);
		nextButton->setObjectName(QStringLiteral("nextButton"));
		nextButton->setGeometry(QRect(260, 170, 75, 23));
		previousButton = new QPushButton(centralWidget);
		previousButton->setObjectName(QStringLiteral("previousButton"));
		previousButton->setGeometry(QRect(50, 170, 75, 23));

		m_uMainWindow->setCentralWidget(centralWidget);

		m_uMainWindow->setWindowTitle(QApplication::translate("MainWindow", "SceneNumber1", Q_NULLPTR));
		nextButton->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
		previousButton->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
	}

	void InitialScene::mf_nextButton()
	{

		int sceneNumber  = lineEdit->text().toInt()+1;
		

		if (sceneNumber >= 1001 || sceneNumber<=0)
		{
			sceneNumber = 1;
			int currentSceneNumber = boost::any_cast<int>(m_TransientDataCollection["SceneNumber"]);
			lineEdit->setText(QString::number(currentSceneNumber));
		}
		m_TransientDataCollection.erase("SceneNumber");
		m_TransientDataCollection.emplace("SceneNumber", sceneNumber);
		std::string c_szNextSceneName = "SceneNumber";
		c_szNextSceneName.append(std::to_string(sceneNumber));
		emit SceneChange(c_szNextSceneName);
	}

	void InitialScene::mf_previousButton()
	{

		
		int sceneNumber = lineEdit->text().toInt() - 1;

		if (sceneNumber <= 0 || sceneNumber >= 1000)
		{
			sceneNumber = 1000;
		}
		int currentSceneNumber = boost::any_cast<int>(m_TransientDataCollection["SceneNumber"]);

		if (sceneNumber == currentSceneNumber)
		{
			lineEdit->setText(QString::number(currentSceneNumber));
		}
		m_TransientDataCollection.erase("SceneNumber");
		m_TransientDataCollection.emplace("SceneNumber", sceneNumber);


		std::string c_szNextSceneName = "SceneNumber";
		c_szNextSceneName.append(std::to_string(sceneNumber));
		emit SceneChange(c_szNextSceneName);
	}
}

