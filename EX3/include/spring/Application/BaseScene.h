#pragma once
#include <spring\Framework\IScene.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qobject.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
		

	private:
		QWidget *centralWidget;
		QLineEdit *lineEdit;
		QPushButton *nextButton;
		QPushButton *previousButton;
		void createGUI();

		private slots :
			void mf_nextButton();
			void mf_previousButton();

	};

}
